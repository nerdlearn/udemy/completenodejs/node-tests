const expect = require('expect');
const utils = require('./utils');

describe('Utils', () => {

  describe('#addition', () => {
    it('should add two numbers', () => {
      var res = utils.add(33, 11);

      /*
      // THIS CODE IS REPACED BY EXPECT
      if(res !== 44){
      throw new Error(`Values do not add up. Sent 33, 11 and expected 44 but got ${res}`);
      }
       */
      expect(res)
      .toBeA('number', `We expected a number but instead got a: ${typeof(res)}`)
      .toBe(44, `Values do not add up. Sent 33, 11 and expected 44 but got ${res}`);

    });

    //USE done in mocha to have the system wait for return
    it('should asynchronously add two numbers', (done) => {
      var res = utils.asyncAdd(7, 4, (sum) => {
          expect(sum).toBe(11).toBeA('number');
          done();
        });

    });
  });
  describe('#square', () => {
    it('should square a number', () => {
      var res = utils.square(3);

      //Long form with custom return messages
      expect(res)
      .toBeA('number', `We expected a number but instead got a: ${typeof(res)}`)
      .toBe(9, `Values do not add up. Sent 33, 11 and expected 44 but got ${res}`);
      // Short form letting Expect handle the messaging
      expect(res).toBe(9).toBeA('number');
    });

    //USE done in mocha to have the system wait for return
    it('should asynchronously square a number', (done) => {
      var res = utils.asyncSquare(7, (square) => {
          expect(square).toBe(49).toBeA('number');
          done();
        });

    });
  });
  //should verify firsta and last names are setActive
  it('should set first and last names in user', () => {
    var blankUser = {
      age: 40,
      location: 40
    };
    var nameToParse = 'Pedro Alvarado';
    var res = utils.setName(blankUser, nameToParse);

    expect(res)
    .toExist()
    .toBeA('object')
    .toInclude({
      firstName: "Pedro"
    })
    .toInclude({
      lastName: "Alvarado"
    });

  });
});
