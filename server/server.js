const express = require('express');

var app = express();

app.get('/', (req, res) => {
  res.status(404).send({
    error: 'Page not found.',
    name: 'Todo App v1.0'
  });
});

// GET /users
// Give users a name prop and an age prop
app.get('/users', (req, res) => {
  res.status(200).send([{
        name: 'Pedro',
        age: 40
      }, {
        name: 'Diana',
        age: 29
      }, {
        name: 'Paz',
        age: 4
      }, {
        name: 'Teo',
        age: 1
      }, ]);
});

app.listen(3000);

module.exports.app = app;
