const request = require('supertest');
const expect = require('expect');

var app = require('./server.js').app;
describe('Server', () => {
  describe('#GET/', () => {
    it('should return Page not found response', (done) => {
      request(app)
      .get('/')
      .expect(404) //Page not found code
      .expect((res) => {
        expect(res.body).toInclude({
          error: 'Page not found.'
        });
      })
      .end(done);
    });
  });
  describe('#GET/users', () => {
    // assert 200
    // assert that YOU exist in array

    it('should return a Page with a users array', (done) => {
      request(app)
      .get('/users')
      .expect(200) // Success code
      .expect((res) => {
        expect(res.body).toInclude({
          name: 'Pedro',
          age: 40
        })
      })
      .end(done);
    });
  });
});
